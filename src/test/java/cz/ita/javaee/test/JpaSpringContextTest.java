package cz.ita.javaee.test;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"jpa"})
public abstract class JpaSpringContextTest extends SpringContextTest {

}
