package cz.ita.javaee.service.api;

public interface NotificationService {
    void sendNotifiction(String title, String text);
}
