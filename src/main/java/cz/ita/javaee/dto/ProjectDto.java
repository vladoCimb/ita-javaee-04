package cz.ita.javaee.dto;

import cz.ita.javaee.type.ProjectStatus;

public abstract class ProjectDto extends AbstractDto {

    private String name;
    private CompanyDto customer;
    private ProjectStatus status = ProjectStatus.OPEN;
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyDto getCustomer() {
        return customer;
    }

    public void setCustomer(CompanyDto customer) {
        this.customer = customer;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
