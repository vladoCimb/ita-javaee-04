package cz.ita.javaee.dto;

import java.util.HashSet;
import java.util.Set;

/**
 * Time material project.
 */
public class TmProjectDto extends ProjectDto {
    private Set<TmOrderDto> orders = new HashSet<>();

    public Set<TmOrderDto> getOrders() {
        return orders;
    }

    public void setOrders(Set<TmOrderDto> orders) {
        this.orders = orders;
    }

}
