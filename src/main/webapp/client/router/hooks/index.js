'use strict';
import bodyClassHook from "./bodyClassHook";
import adminLteFixHook from "./adminLteFixHook";
import {AUTH_ENABLED} from '../../config';

export default (store, router) => {
  if (AUTH_ENABLED) {
    // router.beforeEach(authHook(store));
    router.beforeEach(bodyClassHook());
  }
  router.beforeEach(adminLteFixHook());
};
