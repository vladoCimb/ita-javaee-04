import tmDeliveryResource from '../resources/tmDeliveryResource';
import _ from 'lodash';
import TmDelivery from "../domain/tmDelivery";

export default {
  newTmDelivery(data) {
    return new TmDelivery(data);
  },
  async findAll () {
    const response = await tmDeliveryResource.query();
    if (response.ok) {
      return response.data.map((tmDeliveryData) => new TmDelivery(tmDeliveryData));
    } else {
      return null;
    }
  },
  async create (tmDelivery) {
    const response = await tmDeliveryResource.save({}, _.pickBy(tmDelivery));
    if (response.ok) {
      return new TmDelivery(response.data);
    } else {
      return null;
    }

  },
  async update (tmDelivery) {
    const response = await tmDeliveryResource.update({}, _.pickBy(tmDelivery));
    if (response.ok) {
      return new TmDelivery(response.data);
    } else {
      return null;
    }
  },
  async delete(id) {
    return tmDeliveryResource.delete({id: id});
  }
}
