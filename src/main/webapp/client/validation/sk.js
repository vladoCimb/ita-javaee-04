'use strict';

const messages = {
  _default: (field) => `Neplatná hodnota.`,
  after: (field, [target]) => `Hodnota musí býť neskôr než ${target}.`,
  alpha_dash: (field) => `Pole môže obsahovať len alfanumerické znaky, pomlčky a podtržítka.`,
  alpha_spaces: (field) => `Pole môže obsahovať len alfanumerické znaky.`,
  alpha: (field) => `Pole môže obsahovať len alfa znaky.`,
  before: (field, [target]) => `Hodnota musí býť skôr než ${target}.`,
  between: (field, [min, max]) => `Hodnota musí býť mezi ${min} a ${max}.`,
  confirmed: (field) => `Hodnota pola sa nezhoduje.`,
  credit_card: (field) => `Neplatná hodnota`,
  date_between: (field, [min, max]) => `Dátum musí býť mezi ${min} a ${max}.`,
  date_format: (field, [format]) => `Dátum musí býť vo formáte ${format}.`,
  decimal: (field, [decimals] = ['*']) => `Hodnota musí byť číslo s ${decimals === '*' ? 'ľubovolným počtom desatiných miest.' : 'počtom desatiných miest: ' + decimals}.`,
  decimal2: (field, [decimals] = ['*']) => `Hodnota musí byť číslo s ${decimals === '*' ? 'ľubovolným počtom desatiných miest.' : 'počtom desatiných miest: ' + decimals}.`,
  digits: (field, [length]) => `Hodnota musí obsahovať presne ${length} číslic.`,
  dimensions: (field, [width, height]) => `Maximálne prípustné rozmery obrázku sú ${width} x ${height} pixels (šírka x výška).`,
  email: (field) => `Neplatný formát e-mailové adresy.`,
  ext: (field) => `Neplatný typ súboru.`,
  image: (field) => `Súbor nie je obrázok.`,
  in: (field) => `Neplatná hodnota.`,
  ip: (field) => `Neplatná IP adresa.`,
  max: (field, [length]) => `Maximálna dĺžka textu je ${length}.`,
  max_value: (field, [max]) => `Maximálna hodnota pola je ${max}.`,
  mimes: (field) => `Neplatný typ súboru.`,
  min: (field, [length]) => `Minimálna dĺžka textu je ${length}.`,
  min_value: (field, [min]) => `Minimálna hodnota pola je ${min}.`,
  min_value2: (field, [min]) => `Minimálna hodnota pola je ${min}.`,
  not_in: (field) => `Neplatná hodnota.`,
  numeric: (field) => `Pole môže obsahovať len numerické znaky.`,
  regex: (field) => `Neplatná hodnota.`,
  required: (field) => `Povinné pole.`,
  size: (field, [size]) => `Maximálna veľkosť súboru je ${size} KB.`,
  url: (field) => `Hodnota nie je platná URL.`
};

export default messages;
