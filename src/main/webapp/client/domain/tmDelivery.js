import _ from 'lodash';
import moment from 'moment';
import TmOrder from "./tmOrder";

class TmDelivery {
  constructor(data) {
    _.merge(this, data);
    this.period = data.period ? moment(data.period) : null;
    this.order = data.order ? new TmOrder(data.order) : null;
    this.created = data.created ? moment(data.created) : null;
  }

  get price() {
    return this.mdNumber * this.order.mdRate;
  }

  get sell() {
    return !!this.order.mainOrder.mdRate ? this.mdNumber * this.order.mainOrder.mdRate : 0;
  }

  get profit() {
    return !!this.order.mainOrder ? this.sell - this.price : null;
  }
}

export default TmDelivery;
