import _ from 'lodash';
import moment from 'moment';
import TmDelivery from "./tmDelivery";

class TmOrder {
  constructor(data) {
    _.merge(this, data);
    this.deliveries = data.deliveries ? data.deliveries.map(order => new TmDelivery(order)) : [];
    this.validFrom = data.validFrom ? moment(data.validFrom) : null;
    this.validTo = data.validTo ? moment(data.validTo) : null;
    this.created = data.created ? moment(data.created) : null;

    this.value = this.id;
    this.label = this.number;
  }

  get sum() {
    return (this.mdNumber || 0) * (this.mdRate || 0)
  }

  get deliveriesSum() {
    return _(this.deliveries)
      .map(delivery => delivery.price)
      .sum();
  }

  get profitSum() {
    return _(this.deliveries)
      .map(delivery => delivery.profit)
      .sum();
  }

}

export default TmOrder;
