import _ from 'lodash';
import moment from 'moment';

class Company {
  constructor(data) {
    _.merge(this, data);
    this.created = data.created ? moment(data.created) : null;

    this.label = this.name;
    this.value = !!this.id ?this.id : this.name; //workaround for ares companies where id is null
  }
}

export default Company;
