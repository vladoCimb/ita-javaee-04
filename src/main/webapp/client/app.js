import "bootstrap/dist/js/bootstrap";
import Vue from "vue";
import notificationService from "services/notificationService";
import {sync} from "vuex-router-sync";
import moment from "moment";
import momentSk from "moment/locale/sk";
import numeral from "numeral";
import numeralSk from "numeral/locales/sk";
import vueStrapLang from "vue-strap/dist/vue-strap-lang"; //it must be here because of lang loading
import adminLte from "admin-lte/dist/js/adminlte"; //it must be here for loading adminLte
import App from "./components/app";
import "./resources/";
import router from "./router/";
import store from "./store/";
import interceptors from "./interceptors";
import hooks from "./router/hooks";
import mixins from "./mixins";
import validations from "./validation/";
import filters from "./filters/";

//override moment default toJSON which use UTC time zone, becouse of using LocalDate in BE
moment.fn.toJSON = function() { return this.format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'); };

//set numeral locale to slovak
numeral.locale('sk');
//set moment locale to slovak
moment.locale('sk');

sync(store, router);

//register http interceptor
interceptors(store, router);

//register hooks
hooks(store, router);

//register mixins
mixins(store);

const app = new Vue({
  router,
  store,
  ...App
});

validations();
filters();

notificationService.init();

var AdminLTEOptions = {
  //Enable sidebar expand on hover effect for sidebar mini
  //This option is forced to true if both the fixed layout and sidebar mini
  //are used together
  sidebarExpandOnHover: true,
  //BoxRefresh Plugin
  enableBoxRefresh: true,
  //Bootstrap.js tooltip
  enableBSToppltip: true
};

export {app, router, store}
