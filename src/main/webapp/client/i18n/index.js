'use strict';
import _ from "lodash";
import {vsprintf} from "sprintf-js";

const messages = {
  'error.originalPassword.invalid': 'Nesprávne aktuálne heslo.',
  'error.confirmPassword.invalid': 'Heslá sa nezhodujú.',
  'error.auth.invalid': 'Neoprávnený prístup.',
  'error.auth.login': 'Neplatné prihlasovacie údaje.',
  'error.user.delete.myself': 'Nie je možné zmazať prihláseného užívateľa.',
  'error.user.edit.myself': 'Nie je možné editovať prihláseného užívateľa.',
  'error.company.activate': 'Nie je možné aktovať firmu.',
  'error.company.deactivate': 'Nie je možné deaktovať firmu.',
  'error.employee.activate': 'Nie je možné aktovať zamestnanca.',
  'error.employee.deactivate': 'Nie je možné deaktivovať zamestnanca.',
  'error.tmProject.activate': 'Nie je možné aktovať zamestnanca.',
  'error.tmProject.deactivate': 'Nie je možné deaktivovať zamestnanca.',
  'error.activate.edit.myself': 'Nie je možné aktivovať prihláseného užívateľa.',
  'error.deactivate.edit.myself': 'Nie je možné deaktivovať prihláseného užívateľa.',
  'error.codeList.deactivate.invalid': 'Zneplatnenie položky nebolo možné vykonať.',
  'error.data.invalid': 'Dáta sa nepodarilo spracovať.',
  'error.data.notFound': 'Záznam se nenašiel.',
  'error.data.integrity.violation': 'Akciu nie je možné vykonať, pretože by bola porušená integrita dát.',
  'error.data.integrity.violation.referenced': 'Položku nie je možné odstrániť, pretože je používaná v iných záznamoch.',
  'error.data.integrity.violation.duplicate': 'Akciu nie je možné vykonať. V aplikácii už existuje záznam s rovnakou hodnotou.',
  'error.order.empty': 'Objednávku nie je možné vytvoriť. Musí obsahovať aspoň jendu položku na objednanie.',

  'codeList.delete.confirmation': 'Naozaj chcete odstrániť položku "%s"?',

  'user.delete.confirmation': 'Naozaj chcete odstrániť užívateľa "%s"?',

  'company.delete.confirmation': 'Naozaj chcete odstrániť firmu "%s"?',
  'company.delete.success': 'Firma bola úspešne zmazaná.',
  'company.activate.success': 'Firma bola úspešne aktivovaná.',
  'company.deactivate.success': 'Firma bola úspešne deaktivovaná.',
  'company.edit.success': 'Firma bola uložená.',

  'employee.delete.confirmation': 'Naozaj chcete odstrániť zamestnanca "%s"?',
  'employee.delete.success': 'Zamestnanec bol úspešne zmazaný.',
  'employee.activate.success': 'Zamestnanec bola úspešne aktivovaný.',
  'employee.deactivate.success': 'Zamestnanec bol úspešne deaktivovaný.',
  'employee.edit.success': 'Zamestnanec bol úspešne upravený.',

  'tmProject.delete.confirmation': 'Naozaj chcete odstrániť kontrakt "%s"?',
  'tmProject.delete.success': 'Kontrakt bol úspešne zmazaný.',
  'tmProject.activate.success': 'Kontrakt bola úspešne aktivovaný.',
  'tmProject.deactivate.success': 'Kontrakt bol úspešne deaktivovaný.',
  'tmProject.edit.success': 'Kontrakt bol úspešne upravený.',

  'order.delete.confirmation': 'Naozaj chcete odstrániť objednávku "%s"?',
  'order.delete.success': 'Objednávka bola úspešne zmazaná.',
  'order.edit.success': 'Objednávka bola úspešne upravená.',

  'delivery.delete.confirmation': 'Naozaj chcete odstrániť dodávku?',
  'delivery.delete.success': 'Dodávka bola úspešne zmazaná.',
  'delivery.edit.success': 'Dodávka bola úspešne upravená.'
};

export default {
  message(key, defaultMessageOrObjectArgs, defaultMessageParam) {
    const defaultMessage = _.isString(defaultMessageOrObjectArgs) ? defaultMessageOrObjectArgs : defaultMessageParam;
    let message = messages[key] || (defaultMessage ? (messages[defaultMessage] || defaultMessage) : key);
    if (_.isArray(defaultMessageOrObjectArgs)) {
      message = vsprintf(message, defaultMessageOrObjectArgs);
    }
    return message;
  }
}
