import actions from './employeeActions';
import mutations from './employeeMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
