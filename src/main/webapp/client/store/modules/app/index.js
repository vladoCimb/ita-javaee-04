import actions from './appActions';
import mutations from './appMutations';

const initialState = {
  submitProtection: false,
  configuration: null,
  loadingData: false
};

export default {
  namespaced: true,
  state: initialState,
  mutations: mutations,
  actions: actions
}
