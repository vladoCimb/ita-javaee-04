import tmOrderService from "../../../services/tmOrderService";

const actions = {
  async create ({ dispatch}, tmProject) {
    try {
      let newOrder = await tmOrderService.create(tmProject);
      if (newOrder) {
        await dispatch('tmProject/addOrder', newOrder, { root: true });
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch }, tmProject) {
    try {
      let updatedOrder = await tmOrderService.update(tmProject);
      if (updatedOrder) {
        await dispatch('tmProject/updateOrder', updatedOrder, { root: true });
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await tmOrderService.delete(id);
    if (response.ok) {
      await dispatch('tmProject/deleteOrder', id, { root: true });
      return true;
    } else {
      return false;
    }
  }
};

export default actions
