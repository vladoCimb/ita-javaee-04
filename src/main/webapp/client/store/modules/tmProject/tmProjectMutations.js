import _ from 'lodash';
import { TM_PROJECT_LIST, TM_PROJECT_SELECT, TM_PROJECT_ADD_ORDER, TM_PROJECT_UPDATE_ORDER, TM_PROJECT_DELETE_ORDER,
  TM_PROJECT_ADD_DELIVERY, TM_PROJECT_UPDATE_DELIVERY, TM_PROJECT_DELETE_DELIVERY } from '../../mutationTypes'

const mutations = {
  [TM_PROJECT_LIST](state, action){
    state.items = action.items
  },
  [TM_PROJECT_SELECT](state, action){
    state.selected = action.item
  },
  [TM_PROJECT_ADD_ORDER](state, action){
    state.selected.orders.push(action.order)
  },
  [TM_PROJECT_UPDATE_ORDER](state, action){
    let toBeUpdated = _.find(state.selected.orders, {id: action.order.id});
    _.merge(toBeUpdated, action.order);
  },
  [TM_PROJECT_DELETE_ORDER](state, action){
    state.selected.orders = _.reject(state.selected.orders, {id: action.orderId});
  },
  [TM_PROJECT_ADD_DELIVERY](state, action){
    let order = _.find(state.selected.orders, {id: action.delivery.order.id});
    order.deliveries.push(action.delivery);
  },
  [TM_PROJECT_UPDATE_DELIVERY](state, action){
    let order = _.find(state.selected.orders, {id: action.delivery.order.id});
    let toBeUpdated = _.find(order.deliveries, {id: action.delivery.id});
    _.merge(toBeUpdated, action.delivery);
  },
  [TM_PROJECT_DELETE_DELIVERY]: function (state, action) {
    //find order with deleted delivery
    let order = _.find(state.selected.orders, order => {
      return _.some(order.deliveries, {id: action.deliveryId})
    });

    order.deliveries = _.reject(order.deliveries, {id: action.deliveryId});
  }
};

export default mutations;
