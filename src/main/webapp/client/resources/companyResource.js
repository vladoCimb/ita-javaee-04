import Vue from 'vue';
import {API_ROOT} from '../config'

const customActions = {
  ares: {method: 'GET', url: API_ROOT + 'company/ares'}
};

export default Vue.resource(API_ROOT + 'company{/id}', {}, customActions);
