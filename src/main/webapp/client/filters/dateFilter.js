'use strict';

import moment from 'moment';

export default (value) => {
  return !!value ? moment(value).format("DD.MM.YYYY") : null;
}
